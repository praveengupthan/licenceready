//click event to scroll to top
$("#movetop").click(function () {
  $("html, body").animate({ scrollTop: 0 }, 200);
});


//callback function
$(window).on('load', function () {
  setTimeout(function () { // allowing 3 secs to fade out loader
    $('.page-loader').fadeOut('slow');
  }, 3500);
});

//header add class
$(window).scroll(function () {
  if ($(this).scrollTop() > 50) {
    $(".fixed-top").addClass("sticked");
  } else {
    $(".fixed-top").removeClass("sticked");
  }
});

//on click move to browser top
$(document).ready(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
      $("#movetop").fadeIn();
    } else {
      $("#movetop").fadeOut();
    }
  });
});